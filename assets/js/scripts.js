(function($){


	/* SETTINGS FOR MATT TO PLAY WITH SO YOU DONT BREAK MY ART BECAUSE I SUCK AT DEBUGGING */
	/* url to json file where I will post search query */
    var searchURL = '/Umbraco/Surface/Search/Index'; /* GET request with variable s holding the search string */

	 /* Set to true when things doesn't work and watch browsers console, would be good to set back to false when going live */
	var debug = true;


	function debugLog(m) {
		if ( debug ) {
			console.log( m );
		}
	}

	if ( $('.js-main-slider').length > 0 ) {

		$('.js-main-slider').on('init',function(e,slick,currentSlide,nextSlide){
			var el = slick.$slides[0];
			var allSlides = $('.o-main-slider__item');
			$.each( allSlides , function(i,el){
				eraseSVG( $(el) );
			});
			drawSVG( $(el) );
		});

		$('.js-main-slider').on('beforeChange',function(e,slick,currentSlide,nextSlide){
			var cur = ( nextSlide == undefined ) ? 0 : nextSlide;
			//var el = slick.$slides[cur];
			var allSlides = $('.o-main-slider__item');
			$.each( allSlides , function(i,el){
				eraseSVG( $(el) );
			});
		});	

		$('.js-main-slider').on('afterChange',function(e,slick,currentSlide,nextSlide){
			var cur = ( currentSlide == undefined ) ? 0 : currentSlide;
			var el = slick.$slides[cur];
			drawSVG( $(el) );
		});			

		$('.js-main-slider').slick({
			'arrows'		: false,
			'autoplay'		: true,
			'autoplaySpeed'	: 7500,
			'pauseOnHover'	: false
		});

	}

	if ( $('.js-small-slider').length > 0 ) {
		$('.js-small-slider').slick({
			'arrows'		: false,
			'autoplay'		: true,
			'autoplaySpeed'	: 3500,
			'pauseOnHover'	: false,
			'slidesToShow'	: 5,
			'responsive': [
				{
					breakpoint: 1024,
					settings: {
						slidesToShow: 4,
						slidesToScroll: 2,
					}
				},
				{
					breakpoint: 600,
					settings: {
						slidesToShow: 3,
						slidesToScroll: 1
					}
				},
				{
					breakpoint: 480,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 1
					}
				}
				// You can unslick at a given breakpoint now by adding:
				// settings: "unslick"
			]			
		});		
	}

	/* NAV SUBMENU */
	$('.c-nav__item--parent>a').on('click touchstart',function(e){
		var parent = $(this).parent();
		if ( $('.js-close-subnav').css('display') == 'none' ) { return; }
		parent.addClass('is-submenu-active');
		var submenu = parent.find('.c-nav__submenu');
		var menu = $('.c-nav');

		/* Adjust menu size to accomodate submenu */
		if ( submenu.height() > menu.height() ) {
			var height = submenu.height();
			menu.animate({'height':height+'px'});
		}
		e.preventDefault();
	});

	$('.js-close-subnav').on('click',function(e){
		$('.c-nav__item--parent').removeClass('is-submenu-active');
		/* Reset menu size */
		var menu = $('.c-nav');
		var curHeight = menu.height();
		var autoHeight = menu.css('height','auto').height();
		menu.css('height',curHeight);
		$('.c-nav').animate({'height':autoHeight},function(){ menu.css('height','auto'); });

		e.preventDefault();
		e.stopPropagation();
	});


	if ( $('.js-blog-slider').length > 0 ) {
		$('.js-blog-slider').slick({
			'arrows'		: true,
			'slidesToShow'	: 1,
			'speed'			: 1000	
		});		
	}	


	if ( $('.js-flex-grid').length > 0 ) {
		$('.js-flex-grid').isotope({
			itemSelector	: ".g-item",
			layoutMode		: "masonry",
			masonry: { columnWidth: ".g-item:not(.g-w2)" }
		});
	}

	/* STATUS HANDLERS */
	$('.js-toggle').on('click',function(e){
		var $this = $(this);
		var togglingClass = $this.data('target');
		$('body').toggleClass( togglingClass );


		var action = $this.data('action');
		if ( action != undefined ) {

			switch (action) {
				case "search":
					$('.js-search-input').focus();
					$('body').removeClass( 'is-sidebar-visible' );
					break;
			}

		}

		e.preventDefault();


	});


	$(window).on('scroll',function(){
		var scroll =  $(this).scrollTop();

		if ( scroll > 70 ) {
			$('body').addClass('is-scrolled');
		} else {
			$('body').removeClass('is-scrolled');
		}

	});


	if ( $('.js-contact-f').length > 0 ) {


		$('.js-contact-f').on('focus','input',function(){
			var fieldWrap = $(this).parent();
			fieldWrap.addClass('is-not-empty');
		});

		$('.js-contact-f').on('blur','input',function(){
			var fieldWrap = $(this).parent();

			if ( $(this).val() == "" ) {
				fieldWrap.removeClass('is-not-empty');
			}
		});		

	}


	if ( $('.js-billboard-label').length > 0 ) {
		var billboard = $('.js-billboard-label'),
			bHeight = $('.o-billboard').outerHeight();

		$(window).on('scroll',function(){

			var scroll =  $(this).scrollTop();
			var delay = scroll/2;
			var offset = scroll-delay;
			var opacity = 1 - ( scroll/bHeight );
			opacity = (opacity < 0) ? 0 : opacity;


			billboard.css({
				'transform'	: 'translateY('+offset+'px)',
				'opacity'	: opacity
			});
		
		});
	}


	if ( $('.js-gmap').length > 0 ) {

		var el = $('.js-gmap')[0];
		var lat = 51.356741;
		var lon = -2.201899;

		var isDraggable = $(document).width() > 640 ? true : false;

        var mapOptions = {
            zoom: 10,
            center: new google.maps.LatLng(lat,lon ),
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            mapTypeControl : false,
            zoomControl : true,
            streetViewControl:false,
            scrollwheel : false,
            draggable : isDraggable
        }
        var map = new google.maps.Map(el, mapOptions);

		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(lat,lon ),
			map: map,
			title: 'Bowline',
			icon : '/assets/img/bowline-icon-marker.png'
		});        

	}

	/* IMAGE TO VIDEO */
	if ( $('img[data-video]').length > 0 ) {

		$('img[data-video]').on('click',function(e){

			var $this = $(this),
				originalWidth = $this.outerWidth();
				originalHeight = $this.outerHeight();

			if ( $this.data('video') == "" || $this.data('video') == undefined ) return;

			var video = $("<iframe src='" + $this.data('video') + "' width='"+originalWidth+"' height='"+originalHeight+"' style='height:"+originalHeight+"px;' allowfullscreen />");
			
			$this.parent().addClass('is-video');
			$this.replaceWith( video );	


			e.preventDefault();		

		});

	}
	

	if ( $('.g-item').length > 0 ) {
		$('body').on('touchend',function(){
			$(this).hover();
		});
	}	



	/* SEARCH FUNCTIONALITY */
	$('.js-search-input').on('keyup',function(){
		var $results = $('.js-search-results');
		$results.addClass('is-loading');
		$results.html("");
		var searchString = $(this).val();

		debugLog('Sending search query ('+ searchString +') to '+ searchURL );
		$.get({
			'url'	: searchURL,
			'data'	: {'SearchString' : searchString },
			'dataType' : 'JSON'
		}).done( processResults ).fail(damnIt).always(function(){$results.removeClass('is-loading');});

		function processResults(data) {
			debugLog( 'Success' );

			for ( var key in data ) {
				var results = processSearchData( key , data[key] );
				$results.append( results );
			}

		}


		function damnIt(data){
			if ( data.status && data.status != 200  ) { debugLog( "Response error from server: "+ data.status ); }

			if ( data.status && data.status == 200 ) {
				debugLog( 'Maybe invalid json response see http://jsonlint.com/ if you can fix it if not get in touch and I will do it.' );
			}
			debugLog(data);
		}



	});


	/* MAKING ELEMENTS OUT OF SEARCH DATA */
	function processSearchData(key , data) {

		var returnString = '<h4>' + key + '</h4><ul>';

		$.each(data, function(i,o){

			var defaults = { title : "", meta : "", img : "", url : "" };
			var o = $.extend( defaults , o );

			var img = (o.img) ? "<img src='"+ o.img +"' />" : "";

			returnString += "<li><a href='"+ o.url +"'>"+img+"<strong>"+o.title+"</strong><span>"+o.meta+"</span></a></li>";

		});

		returnString += "</ul>";

		return $(returnString);

	}

	/* SEARCH FUNCTIONALITY END */



	/* Drawing svg */
	function drawSVG(el) {

		var duration = 3000;

		if ( el === undefined ) return;

		/* initialize before drawing */
		eraseSVG( el );

		var p = el.find('.js-draw');

		if ( p.length == 0 ) return;


		var totalLength = 0;
		$.each(p,function(i,e){
			var $e = $(e);

			if ( e.nodeName != "path" ) return;

			var length = e.getTotalLength();
			totalLength += length;
	

		});
		/* Calculate duration for one unit of length */
		var durationFraction = duration/totalLength;


		draw(0);


		function draw(i) {

			var $e = p.eq(i);

			$e.css('opacity',1);
			if ( $e.data('gradient') == undefined && p[i].nodeName == "path" ) {
				var length = p[i].getTotalLength();
				var dur = durationFraction * length;
				var targetOffset = 0;

				if ( $e.data('reverse') != undefined ) {
					targetOffset = length*2;
				}	
			

				$e.animate(
					{'stroke-dashoffset' : targetOffset},
					{
						'duration' : dur,
						'step'	   : function(now){ window.requestAnimationFrame(function(){ frame(now); }); } 
					}
				);		
				
				function frame(now){ $e.attr('stroke-dashoffset',now+'px'); }	

			} else if ( p[i].nodeName != "path" && $e.data('r') ) {
				$e.attr( 'r' , $e.data('r') );
			} else {
				var length = p[i].getTotalLength();
				var dur = durationFraction * length;
				var grad = $e.data('gradient');
				var $grad = $(grad);

				var $stop = $grad.find('stop');			
				$stop.animate(
					{'offset' : '0'},
					{
						'duration' : dur,
						'step'	   : function(now){ $(this).attr('offset',now+'%'); } 
					}
				);

			}


			i++;
			if ( p.length > i ) {
				setTimeout( draw.bind(drawSVG,i) ,dur);
			}	
		}

	}

	/* Not literally, just prepare for being drawn */
	function eraseSVG(el) {
		var $e = el.find('.js-draw');

		if ( $e.length == 0 ) return;		

		$.each($e,function(i,e){
			var $e = $(e);

			$e.css('opacity',0);
			/* Circles */
			if ( $e.data('r') != undefined ) {
				$e.attr('r' , 0);
			}

			if ( e.nodeName != "path" ) return;
			/* Stroke Paths */
			if ( $e.data('gradient') == undefined ) {
				var length = e.getTotalLength();
				$e.css({
					'stroke-dasharray' : length,
					'stroke-dashoffset' : length
				});	


			/* Gradient paths */
			} else {
				var grad = $e.data('gradient');
				var stop = $(grad).find('stop');
				stop.attr('offset',0);
				$e.css({'fill':'url('+grad+')'});
			}				

		});
	}


})(jQuery)