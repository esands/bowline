module.exports = function(grunt) {

    // 1. All configuration goes here 
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),


        includes: {
            files: {
                src: ['build/*.html'], // Source files
                dest: './', // Destination directory
                flatten: true,
                debug : false,
                cwd: '.',
                options: {
                    silent: true,
                    banner: '<!-- INCLUDE <% includes.files.dest %> -->'
                }
            }
        },

        imagemin: {
            dynamic: {
                files: [{
                    expand: true,
                    cwd: 'assets/img/',
                    src: ['**/*.{png,jpg,gif}'],
                    dest: 'assets/img/'
                }]
            }
        },
        
        sass: {
            live: {
                options: {
                    sourcemap: 'inline',
                    style: 'compressed',
                    compass: true
                },
                files: {
                    'assets/css/main.css': 'assets/sass/main.scss'
                }
            }, 
            dev: {
                options: {
                    sourcemap: 'inline',
                    style : 'expanded',
                    compass: true
                },
                files: {
                    'assets/css/main.css': 'assets/sass/main.scss'
                }
            } 
        },          
        
        postcss: {
          options: {
            map: true,
            processors: [
              require('autoprefixer')({browsers: ['last 10 versions', 'ie 10']})
            ]
          },
          dist: {
            src: 'assets/css/main.css'
          }
        },        
        
        watch: {
            
            css: {
                files: ['assets/sass/*.scss'],
                tasks: ['sass:dev','postcss'],
                options: {
                    spawn: false
                }
            },
            html: {
                files: ['build/*.html','build/inc/*.html','build/inc/svg/*.svg','build/inc/svg/*/*.svg'],
                tasks: ['includes'],
                options : {
                    spawn: false
                }
            }
        },
        
        browserSync: {
            bsFiles: {
                src : ['assets/css/*.css','*.html','build/*.html']
            },
            options: {
                watchTask: true,
                server: {
                    baseDir: "./"
                }
            }
        }      

    });

    // 3. Where we tell Grunt we plan to use this plug-in.
    grunt.loadNpmTasks('grunt-includes');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-browser-sync');
    grunt.loadNpmTasks('grunt-contrib-imagemin');  
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-postcss');
    //grunt.loadNpmTasks('autoprefixer');
    // 4. Where we tell Grunt what to do when we type "grunt" into the terminal.
    grunt.registerTask('live', ['imagemin','sass:live','postcss']);
    grunt.registerTask('default', ['browserSync','watch','sass:dev']);

};